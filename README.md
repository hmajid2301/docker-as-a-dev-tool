# My Journey Using Docker as a Development Tool

Code + slides for conference talk.

**NEW REPO:** https://gitlab.com/hmajid2301/talks

## Getting Started

To launch the presentation. You can edit locally and the live-reloader
will handle updating the slides automatically.

```
make start

# Go to http://localhost:8000
```

## Appendix

- Icons from https://www.flaticon.com/